# Labyrinthe

un agent capable de s’échapper d’un labyrinthe en appliquant en suivant le mur de droite (à droite) ou en suivant le mur de gauche (à gauche). Les "#" sont des murs, l’entrée du labyrinthe est en haut à gauche, la sortie en bas à droite, les "-" marquent les cases visitées. 

A project implemented in 2018


To run the code:
```bash
python3 labyrinthe.py file_labyrinthe g|d
```
