"""
Labyrinthe
Labyrinthe résolu en suivant le mur de gauche (à gauche) et en suivant le mur de droite (à droite).
Les "#" sont des murs, l’entrée du labyrinthe est en haut à gauche, la sortie en bas à droite,
les "-" marquent les cases visitées. ce projet demande un agent capable de s’échapper d’un labyrinthe en suivrant soit
le mur gauche, soit le mur droit en fonction d’un second argument fourni lors de l’appel au programme.
"""
_author_="Zheng Ren"
_date_="07/11/2018"

import sys, turtle
file,gd=sys.argv[1],sys.argv[2]
MSG_BOUCLE = "Boucle détectée, cases visitées:"
MSG_SORTIE = "Sortie trouvée, cases visitées:"
hauteur=0     #hautuer de labyrinthe initialisé à zéro
x,y=-300,200  # position de départ pour dessiner le labyrinthe
taille=40     #la taille de chaque mur(carré)
dir="sud"     #l'orientation au départ de l'agent
pos=(1,-1)    #postion de départ de l'agent
dico_case={}  #dictionnaire contient de chaque position où l'agent a été passé et orientation conrrespondente
case=[]       #une liste de cases visitées
list_droite=["nord","est","sud","ouest"]  #la list d'orientation pour tourner à droite
list_gauche=["nord","ouest","sud","est"]  #la list d'orientation pour tourner à gauche
fleche=turtle.Turtle()
with open(file)as f:
    for chaine in f:
        hauteur += 1
        largeur = len(chaine.strip())

matrice=[[" " for i in range(largeur)]for j in range(hauteur)]

with open(file)as f:
    ligne = 0
    for s in f:
        r=s.strip()
        for j in range(len(r)):
            matrice[ligne][j]=r[j]
        ligne+=1

def print_matrice(): #affiche le labyrinthe
    for i in range(len(matrice)):
        for j in range(len(matrice[0])):
            print(matrice[i][j],end="")
        print()
    print()

def direcion(dir):
    """
    renvoie la flèche qui indique la direction de l'agent selon la direction en paramètre
    :param dir: l'orientation de l'agent
    """
    dico_direction = {"nord": "^", "ouest": "<", "est": ">", "sud": "v"}
    return dico_direction[dir]

def prochain(dir,cote):
    """
    renvoie la direction à gauche ou à droite de la direction actuelle selon la liste(lst) fournie en paramètre
    :param dir: direction actuelle de l'agent
    :param cote: le côté qu'on veut ragarder droite ou gauche
    """
    lst=list_gauche
    if(cote=="d"):
        lst = list_droite
    pr=(lst.index(dir)+1+4)%4
    return lst[pr]

def droite_gauche(dir,pos,cote):
    """
    renvoie la position de la case à droite ou à gauche de l'agent
    :param cote:le côté qu'on veut regarder(droite ou gauche)
    :param dir:l'orientation de l'agent
    :param pos:la case dans laquelle l'agent se trouve, avec pos(colonne,ligne)
    """
    lst = [-1, 1]
    if (cote == "g"):
        lst = [1, -1]
    i, j = pos
    dico_case = {"nord": (i + lst[1], j), "ouest": (i, j + lst[0]), "est": (i, j + lst[1]), "sud": (i + lst[0], j)}
    return dico_case[dir]

def devant(dir,pos):
    """
    renvoie la position de la case devant l'agent
    :param dir: l'orientation de l'agent
    :param pos:la case dans laquelle l'agent se trouve, avec pos(colonne,ligne)
    """
    i, j = pos
    res=(-1,-1)
    if(dir=="nord" and j!=0):
        res=(i,j-1)
    elif(dir=="est"):
        res=(i+1,j)
    elif(dir=="ouest"):
        res = (i-1, j)
    elif(dir=="sud" and j!=hauteur-1):
        res = (i, j + 1)
    return res

def tourner(dir,pos,gd):
    """
    renvoie l'orientation de l'agent lorsqu'il suit le mur droite ou gauche selon gd indique
    :param dir:l'orientation actuelle de l'agent
    :param pos:la case dans laquelle l'agent se trouve, avec pos(colonne,ligne)
    :param le côté de mur que l'agent va suivre pour trouver la sortie
    """
    devant_col,devant_lign=devant(dir,pos)
    res=dir
    autre_cote="d"
    gd_col, gd_lig = droite_gauche(dir, pos, "g")
    if(gd=="d"):
        gd_col, gd_lig = droite_gauche(dir, pos, "d")
        autre_cote = "g"
    if (matrice[gd_lig][gd_col] == " "):
        res = prochain(res,gd)
        tourner_turtle(gd)
    else:
        while (matrice[devant_lign][devant_col] != " " or devant_lign==-1):
            res = prochain(res,autre_cote)
            tourner_turtle(autre_cote)
            devant_col,devant_lign = devant(res, pos)
    return res

def move(dir,pos):
    """
    renvoie la case que l'agent va se déplacer
    :param dir:l'orientation actuelle de l'agent
    :param pos:la case dans laquelle l'agent se trouve acturellement, avec pos(colonne,ligne)
    """
    i, j = pos
    if j>=0:
        matrice[j][i]=" "
    dico_move = {"nord": (i, j - 1), "sud": (i, j + 1), "est": (i + 1, j), "ouest": (i - 1, j)}
    pos = dico_move[dir]
    matrice[pos[1]][pos[0]] = direcion(dir)
    return pos

def gagne(pos):
    """
    renvoie True si l'agent atteint la sortie, la sortie est en position (largeur - 2, hauteur -1) sinon False
    :param pos:la case dans le labyrinthe où se trouve l'agent, avec pos(colonne,ligne)
    """
    return pos[0]==largeur-2 and pos[1]==hauteur-1

def boucle(pos,dir,dico):
    """
    renvoie True si l'agent tourne en rond (i.e si il se trouve deux fois sur la même case avec la même orientation)
    :param pos:la case dans le labyrinthe où se trouve l'agent, avec pos(colonne,ligne)
    :param dir:l'orientation de l'agent
    :param dico: dictionnaire contient de chaque position où l'agent a été passé et orientation conrrespondente
    """
    return pos in dico and dir in dico[pos]

def cases_visite(case):
    """
    metttre à jour le labyrinthe en renplaçant toutes les cases visitées par "-"
    :param case: une liste de cases visitées
    """
    for visite in range(len(case)):
        i,j=case[visite]
        matrice[j][i]="-"

def carre(x,y):# dessine un carré rempli de longueur taille en position (x,y).
    fleche.up()
    fleche.goto(x,y)
    fleche.down()
    fleche.begin_fill()
    for i in range(4):
        fleche.forward(taille)
        fleche.right(90)
    fleche.end_fill()
    fleche.up()

def labyrinthe(x,y): #dessine le labyrinthe avec turtle avec les coordonnés x,y:le point en haut à gauche du carré
    fleche.speed(0)
    for i in range(len(matrice)):
        pos_x =x
        pos_y = y-taille*i
        for j in range(len(matrice[0])):
            if(matrice[i][j]!=" "):
                carre(pos_x,pos_y)
            pos_x += taille

def tourner_turtle(cote):#turtle tourne selon le côté donné dans le paramètre
    if(cote=="d"):
        fleche.right(90)
    else:
        fleche.left(90)

def chemin(dis): # dessine le chemin parcour de l'agent avec dis: la distance de chaque pas
    fleche.speed(1)
    fleche.down()
    fleche.forward(dis)

labyrinthe(x,y)
fleche.goto(x+taille*1.5,y)
fleche.right(90)
fleche.shape("turtle")
chemin(taille/2)
while(not boucle(pos,dir,dico_case) and not gagne(pos)):
    if (pos not in dico_case):
        dico_case[pos] = [dir]
    else:
        dico_case[pos].append(dir)
    dir=tourner(dir,pos,gd)
    pos=move(dir,pos)
    if (len(case) > 0):
        chemin(taille)
    case.append(pos)
    print_matrice()
if gagne(pos):
    print(MSG_SORTIE)
else:
    print(MSG_BOUCLE)
print(case)
cases_visite(case)
print_matrice()
x=input('tapez return pour terminer : ')
